<?php
/**
 * Created by PhpStorm.
 * User: slawek
 * Date: 28.02.18
 * Time: 11:46
 */

namespace Dojo;


class FileParser
{

    public function parse($param)
    {
        $lineParser = new LineParser();

        $numbers = [];
        foreach( explode("\n\n", $param) as $line){
            $numbers[] = $lineParser->parse($line);
        }

        return $numbers;
    }
}