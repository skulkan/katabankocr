<?php
/**
 * Created by PhpStorm.
 * User: slawek
 * Date: 28.02.18
 * Time: 11:59
 */

namespace Dojo;


class ChecksumValidator
{
    public function validate($accountNumber)
    {
        $checksum = 0;
        $digits = str_split($accountNumber);
        foreach ($digits as $index => $digit) {
            $checksum += (9 - $index) * $digit;
        }
        return $checksum % 11 === 0;
    }

    public function getCode()
    {
        return 'ERR';
    }
}