<?php
/**
 * Created by PhpStorm.
 * User: slawek
 * Date: 28.02.18
 * Time: 11:59
 */



namespace Dojo;


class ValidateCol
{
    public function validate($numbers)
    {
        $res = [];
        $validators = [new IllegalValidator(), new ChecksumValidator()];

        foreach ($numbers as $number) {
            $res[$number] = '';
            foreach ($validators as $validator) {
                if (!$validator->validate($number)) {
                    $res[$number] = $validator->getCode();

                    break;
                }
            }
        }

        return $res;
    }
}