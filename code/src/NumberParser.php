<?php
/**
 * Created by PhpStorm.
 * User: slawek
 * Date: 28.02.18
 * Time: 10:39
 */

namespace Dojo;


class NumberParser
{
    const NUMBERS = [
        " _ ".
        "| |" .
        "|_|" => '0',

        "   ".
        "  |" .
        "  |" => '1',

        " _ ".
        " _|" .
        "|_ " => '2',

        " _ ".
        " _|" .
        " _|" => '3',

        "   ".
        "|_|" .
        "  |" => '4',

        " _ ".
        "|_ ".
        " _|" => '5',

        " _ ".
        "|_ ".
        "|_|" => '6',

        " _ ".
        "  |".
        "  |" => '7',

        " _ ".
        "|_|".
        "|_|" => '8',

        " _ ".
        "|_|".
        " _|" => '9',
    ];
    /**
     * Parse number
     *
     * @param $string
     * @return string
     */
    public function parse($string)
    {
        return self::NUMBERS[$string] ?? '?';
    }
}