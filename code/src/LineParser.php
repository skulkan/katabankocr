<?php
/**
 * Created by PhpStorm.
 * User: slawek
 * Date: 28.02.18
 * Time: 11:05
 */

namespace Dojo;


class LineParser
{
    public function parse($lines)
    {
        $parser = new NumberParser();
        $parts = explode("\n", $lines);

        $numbers = [];
        for ($i = 0; $i <9 ; $i++) {
            $numberStr =
                substr($parts[0], $i*3, 3).
                substr($parts[1], $i*3, 3).
                substr($parts[2], $i*3, 3);

            $numbers[] = $parser->parse($numberStr);
        }

        return join('', $numbers);
    }
}