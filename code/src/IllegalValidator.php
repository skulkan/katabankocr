<?php
/**
 * Created by PhpStorm.
 * User: slawek
 * Date: 28.02.18
 * Time: 12:24
 */

namespace Dojo;


class IllegalValidator
{
    public function validate($accNumber)
    {
        return strpos($accNumber, '?') === false;
    }


    public function getCode()
    {
        return 'ILL';
    }
}