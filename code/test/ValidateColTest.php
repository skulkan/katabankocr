<?php

use Dojo\ValidateCol;
use PHPUnit\Framework\TestCase;

class ReportTest extends TestCase
{

    public function numberProvider()
    {
        return [
            [
                [
                    '457508000',
                    '664371495',
                    '12345?089'
                ],
                [
                    '457508000' => '',
                    '664371495' => 'ERR',
                    '12345?089' => 'ILL',
                ]
            ]
        ];
    }

    /**
     * @dataProvider numberProvider
     * @param $numbers
     * @param $expected
     */
    public function testValidateCol($numbers, $expected)
    {
        $validate = new ValidateCol();
        $result = $validate->validate($numbers);

        $this->assertEquals($expected, $result);
    }
}
