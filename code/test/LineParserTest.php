<?php

use Dojo\LineParser;
use PHPUnit\Framework\TestCase;

class LineParserTest extends TestCase
{

    public function lineProvider()
    {
        $line1 = <<<ble
    _  _     _  _  _  _  _ 
  | _| _||_||_ |_   ||_||_|
  ||_  _|  | _||_|  ||_| _|

ble;

        $line2 = <<<ble
    _  _     _  _  _  _  _ 
  | _| _||_||_ |_ | ||_||_|
  ||_  _|  | _||_||_||_| _|

ble;

        return [
            [$line1, '123456789'],
            [$line2, '123456089'],
        ];
    }

    /**
     * @dataProvider lineProvider
     * @param $line
     * @param $expected
     */
    public function testParse($line, $expected)
    {
        $parser = new LineParser();
        $this->assertEquals($expected, $parser->parse($line));
    }
}
