<?php

use Dojo\FileParser;
use PHPUnit\Framework\TestCase;

class FileParserTest extends TestCase
{

    public function fileProvider()
    {
        $file1 = <<<ble
    _  _     _  _  _  _  _ 
  | _| _||_||_ |_   ||_||_|
  ||_  _|  | _||_|  ||_| _|

    _  _     _  _  _  _  _ 
  | _| _||_||_ |_ | ||_||_|
  ||_  _|  | _||_||_||_| _|

ble;

        return [
            [$file1, ['123456789', '123456089']],
        ];
    }

    /**
     * @dataProvider fileProvider
     * @param $file
     * @param $expected
     */
    public function testParse($file, $expected)
    {
        $parser = new FileParser();
        $this->assertEquals($expected, $parser->parse($file));
    }
}
