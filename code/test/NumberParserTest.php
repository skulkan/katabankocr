<?php

use Dojo\NumberParser;
use PHPUnit\Framework\TestCase;

class NumberParserTest extends TestCase
{

    public function numberProvider()
    {
        $digit0 =
            " _ ".
            "| |" .
            "|_|";

        $digit1 =
            "   ".
            "  |" .
            "  |";

        $digit2 =
            " _ ".
            " _|" .
            "|_ ";

        $digit3 =
            " _ ".
            " _|" .
            " _|";

        $digit4 =
            "   ".
            "|_|" .
            "  |";

        $digit5 =
            " _ ".
            "|_ ".
            " _|";

        $digit6 =
            " _ ".
            "|_ ".
            "|_|";

        $digit7 =
            " _ ".
            "  |".
            "  |";

        $digit8 =
            " _ ".
            "|_|".
            "|_|";

        $digit9 =
            " _ ".
            "|_|".
            " _|";

        $illegal =
            " _ ".
            "| |".
            " _|";


        return [
            [$digit0, '0'],
            [$digit1, '1'],
            [$digit2, '2'],
            [$digit3, '3'],
            [$digit4, '4'],
            [$digit5, '5'],
            [$digit6, '6'],
            [$digit7, '7'],
            [$digit8, '8'],
            [$digit9, '9'],
            [$illegal, '?'],
        ];
    }


    /**
     * Test Parse
     *
     * @dataProvider numberProvider
     * @param $param
     * @param $expected
     */
    public function testParse($param, $expected)
    {
        $parser = new NumberParser();
        $this->assertEquals($expected, $parser->parse($param));
    }


}
