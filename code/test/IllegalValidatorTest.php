<?php

use Dojo\IllegalValidator;

/**
 * Created by PhpStorm.
 * User: slawek
 * Date: 28.02.18
 * Time: 12:21
 */
class IllegalValidatorTest extends \PHPUnit\Framework\TestCase
{
    public function validateProvider()
    {
        return [
            ['664371495', true],
            ['86110??36', false],
        ];
    }

    /**
     * @dataProvider validateProvider
     */
    public function testValidate($accNumber, $exp)
    {
        $validator = new IllegalValidator();

        $result = $validator->validate($accNumber);

        $this->assertEquals($exp, $result);
    }
}