<?php

use Dojo\ChecksumValidator;
/**
 * Created by PhpStorm.
 * User: slawek
 * Date: 28.02.18
 * Time: 11:55
 */
class ChecksumValidatorTest extends \PHPUnit\Framework\TestCase
{

    public function validateProvider()
    {
        return [
            ['345882865', true],
            ['345882864', false],
            ['111111111', false],
        ];
    }

    /**
     * @dataProvider validateProvider
     *
     * @param $accountNumber
     * @param $expected
     */
    public function testValidate($accountNumber, $expected)
    {
        $validator = new ChecksumValidator();

        $result = $validator->validate($accountNumber);

        $this->assertEquals($expected, $result);
    }
}