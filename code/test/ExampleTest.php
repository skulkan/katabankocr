<?php

use Dojo\Example;
use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    public function testHello()
    {
        $example = new Example();

        $this->assertEquals('Hello world!', $example->hello());
    }
}